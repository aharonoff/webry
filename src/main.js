var colors = {
  black: [0, 0, 0],
  white: [255, 255, 255]
}

var video
var poseNet
var pose
var skeleton
var fonts = []
var states = {
  welcome: true,
  pasting: false,
  start: false,
  placing: false,
  editing: false
}
var texts = {
  welcome: 'this is a webcam application for making concrete poetry   ',
  howto: 'the copied text will be placed word-by-word, however you write certain parts inside s><s to make them placed letter-by-letter      ',
  paste: 'raise both hands above your head to paste text from clipboard      ',
  pasted: undefined,
}

function preload() {
  fonts.push(loadFont('ttf/UbuntuMono-Regular.ttf'))
  fonts.push(loadFont('ttf/UbuntuMono-Italic.ttf'))
  fonts.push(loadFont('ttf/UbuntuMono-Bold.ttf'))
  fonts.push(loadFont('ttf/UbuntuMono-BoldItalic.ttf'))
}

function setup() {
  video = createCapture(VIDEO)
  video.hide()
  poseNet = ml5.poseNet(video, modelLoaded)
  poseNet.on('pose', gotPoses)
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  background(colors.black)
  frameRate(60)

  // welcome screen
  if (states.welcome === true) {
    noFill()
    stroke(colors.white)
    strokeWeight(4)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    ellipse(0, 0, 376)
    if (dist(mouseX, mouseY, windowWidth * 0.5, windowHeight * 0.5) <= 128) {
      fill(colors.white)
    }
    ellipse(0, 0, 128)
    pop()

    // welcome text
    for (var i = 0; i < texts.welcome.length; i++) {
      textFont(fonts[2])
      textAlign(CENTER, CENTER)
      textSize(24)
      fill(colors.white)
      noStroke()
      push()
      translate(windowWidth * 0.5 - sin(Math.PI * 2 * (i / texts.welcome.length) + Math.PI - frameCount * 0.005) * 128, windowHeight * 0.5 + cos(Math.PI * 2 * (i / texts.welcome.length) + Math.PI - frameCount * 0.005) * 128)
      push()
      rotate(Math.PI * 2 * (i / texts.welcome.length) - frameCount * 0.005)
      text(texts.welcome.split('')[i], 0, 0)
      pop()
      pop()
    }

    // instruction text
    noFill()
    stroke(colors.white)
    strokeWeight(32)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    arc(0, 0, 504, 504, Math.PI * 0.785- frameCount * 0.005, Math.PI * 0.825 - frameCount * 0.005)
    pop()
    for (var i = 0; i < texts.howto.length; i++) {
      textFont(fonts[2])
      textAlign(CENTER, CENTER)
      textSize(24)
      fill(colors.white)
      noStroke()
      push()
      translate(windowWidth * 0.5 - sin(Math.PI * 2 * (i / texts.howto.length) + Math.PI - frameCount * 0.005) * 256, windowHeight * 0.5 + cos(Math.PI * 2 * (i / texts.howto.length) + Math.PI - frameCount * 0.005) * 256)
      push()
      rotate(Math.PI * 2 * (i / texts.howto.length) - frameCount * 0.005)
      if (i >= 84 && i <= 87) {
        fill(colors.black)
      }
      text(texts.howto.split('')[i], 0, 0)
      pop()
      pop()
    }
  }

  if (pose) {
    if (states.pasting === true) {
      push()
      translate((windowWidth + video.width) * 0.5, (windowHeight - video.height) * 0.5)
      scale(-1, 1)
      noFill()
      stroke(colors.white)
      strokeWeight(4)
      // head
      ellipse(pose.nose.x, pose.nose.y, 256, 256)
      // eyes --> right && left
      ellipse(pose.nose.x - 52, pose.nose.y - 32, 24, 24)
      ellipse(pose.nose.x + 52, pose.nose.y - 32, 24, 24)
      // nose
      noFill()
      stroke(colors.white)
      strokeWeight(4)
      line(pose.nose.x, pose.nose.y + 36, pose.nose.x, pose.nose.y)
      // if (pose.nose.y >= pose.rightWrist.y && pose.nose.y >= pose.leftWrist.y) {
      //   arc(pose.nose.x, pose.nose.y + 24, 128, 128, Math.PI * 0.1, Math.PI * 0.9)
      //   // ellipse(pose.nose.x, pose.nose.y + 64, 36)
      // } else {
      //   arc(pose.nose.x, pose.nose.y + 24, 128, 128, Math.PI * 0.25, Math.PI * 0.75)
      // }
      // mouth
      arc(pose.nose.x, pose.nose.y + 24, 128, 128, Math.PI * 0.25, Math.PI * 0.75)
      pop()

      // instruction text
      for (var i = 0; i < texts.paste.length; i++) {
        textFont(fonts[2])
        textAlign(CENTER, CENTER)
        textSize(24)
        fill(colors.white)
        noStroke()
        push()
        translate((windowWidth + video.width) * 0.5 - pose.nose.x - sin(Math.PI * 2 * (i / texts.paste.length) + Math.PI - frameCount * 0.005) * 156, (windowHeight - video.height) * 0.5 + pose.nose.y + cos(Math.PI * 2 * (i / texts.paste.length) + Math.PI - frameCount * 0.005) * 156)
        push()
        rotate(Math.PI * 2 * (i / texts.paste.length) - frameCount * 0.005)
        text(texts.paste.split('')[i], 0, 0)
        pop()
        pop()
      }

      // hands --> right && left
      push()
      translate((windowWidth + video.width) * 0.5, (windowHeight - video.height) * 0.5)
      scale(-1, 1)
      // let earR = pose.rightEar
      // let earL = pose.leftEar
      // let d = dist(earR.x, earR.y, earL.x, earL.y)
      noFill()
      stroke(colors.white)
      strokeWeight(4)
      // ellipse(pose.nose.x, pose.nose.y, d)
      // fill(colors.white)
      ellipse(pose.rightWrist.x, pose.rightWrist.y, 64)
      ellipse(pose.leftWrist.x, pose.leftWrist.y, 64)
      pop()

      // https://whatwebcando.today/clipboard.html
      // https://www.sitepoint.com/clipboard-api/
      // https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Interact_with_the_clipboard
      // https://stackoverflow.com/questions/6413036/get-current-clipboard-content
      // getting text from clipboard
      if (pose.nose.y >= pose.rightWrist.y && pose.nose.y >= pose.leftWrist.y) {
        navigator.clipboard.readText()
          .then(text => {
            texts.pasted = text
            // console.log('Pasted content: ', text)
            // console.log(texts.pasted)
          })
          // pop-up error --> focus on window
          // .catch(err => {
          //   console.error('Failed to read clipboard contents: ', err)
          // })
      }
      // updating state
      if (texts.pasted !== undefined) {
        states.pasting = false
        states.start = true
      }
    }

    // start placing text
    if (states.start === true) {
      noFill()
      stroke(colors.white)
      strokeWeight(4)
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      ellipse(0, 0, 256)
      pop()
    }

    // push()
    // translate((windowWidth + video.width) * 0.5, (windowHeight - video.height) * 0.5)
    // scale(-1, 1)
    // let earR = pose.rightEar
    // let earL = pose.leftEar
    // let d = dist(earR.x, earR.y, earL.x, earL.y)
    // noFill()
    // stroke(colors.white)
    // strokeWeight(4)
    // // ellipse(pose.nose.x, pose.nose.y, d)
    // // fill(colors.white)
    // ellipse(pose.rightWrist.x, pose.rightWrist.y, 64)
    // ellipse(pose.leftWrist.x, pose.leftWrist.y, 64)

    // noFill()
    // stroke(colors.white)
    // strokeWeight(4)
    // // neck
    // line(pose.leftShoulder.x + (pose.rightShoulder.x - pose.leftShoulder.x) * 0.5, pose.leftShoulder.y + (pose.rightShoulder.y - pose.leftShoulder.y) * 0.5, pose.nose.x, pose.nose.y + d * 0.5)
    // // body
    // line(pose.leftShoulder.x, pose.leftShoulder.y, pose.rightShoulder.x, pose.rightShoulder.y)
    // line(pose.leftHip.x, pose.leftHip.y, pose.rightHip.x, pose.rightHip.y)
    // line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftHip.x, pose.leftHip.y)
    // line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightHip.x, pose.rightHip.y)
    // // right arm
    // line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightElbow.x, pose.rightElbow.y)
    // line(pose.rightElbow.x, pose.rightElbow.y, pose.rightWrist.x, pose.rightWrist.y)
    // // left arm
    // line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftElbow.x, pose.leftElbow.y)
    // line(pose.leftElbow.x, pose.leftElbow.y, pose.leftWrist.x, pose.leftWrist.y)
    // // right leg
    // line(pose.rightHip.x, pose.rightHip.y, pose.rightKnee.x, pose.rightKnee.y)
    // line(pose.rightKnee.x, pose.rightKnee.y, pose.rightAnkle.x, pose.rightAnkle.y)
    // // left leg
    // line(pose.leftHip.x, pose.leftHip.y, pose.leftKnee.x, pose.leftKnee.y)
    // line(pose.leftKnee.x, pose.leftKnee.y, pose.leftAnkle.x, pose.leftAnkle.y)
    // pop()

    textFont(fonts[2])
    fill(colors.white)
    noStroke()
    textSize(24)
    textAlign(LEFT, CENTER)
    text('distance: ' + Math.floor(dist(pose.leftWrist.x, pose.leftWrist.y, pose.rightWrist.x, pose.rightWrist.y)), 24, 32)
    text('head: (' + Math.floor((windowWidth + video.width) * 0.5 - pose.nose.x) + ', ' + Math.floor((windowHeight - video.height) * 0.5 + pose.nose.y) + ')', 24, 2 * 32)
  }
}

function mousePressed() {
  if (states.welcome === true) {
    if (dist(mouseX, mouseY, windowWidth * 0.5, windowHeight * 0.5) <= 128) {
      states.welcome = false
      states.pasting = true
    }
  }
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose
    skeleton = poses[0].skeleton
  }
}

function modelLoaded() {
  console.log('poseNet ready')
}
